<br><br><br><br>
<div class="col-md-12 table-responsive">
    <h1>Lista de mensajes</h1>
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr class="active">
                <th>Nombre</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Mensaje</th>
                <th>Fecha</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr class="success" ng-repeat="mensaje in Mensajes | limitTo:5 | orderBy:'-Date'">
                <td>{{ mensaje.Nombre }}</td>
                <td>{{ mensaje.Email }}</td>
                <td>{{ mensaje.Telefono }}</td>
                <td>{{ mensaje.Contenido }}</td>
                <td>{{ mensaje.Fecha }}</td>
                <td><button type="button" ng-click="eliminarMensaje(mensaje.MensajeId)"class="btn btn-danger">Eliminar<span class="glyphicon glyphicon-trash"></span></button></td>        
            </tr>
        </tbody>        
    </table>
</div>