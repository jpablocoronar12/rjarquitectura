<?php
    if($Vars){
        $Error=$Vars->ErrorMessage;
        echo "<script>alert('$Error');</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">       
        <title>Inicio de sesión</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/login.css" rel="stylesheet">      
    </head>
    <body>      
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form class="form form-signin" role="form" action="login.do" method="POST">
                        <div class="form-group form-login">
                            <h2 class="wrapper">Inicio de sesi&oacute;n <br>RJ Arquitectura.</h2>
                            <strong><hr></strong>
                            <input type="text" name="user" class="form-control input-md chat-input" placeholder="Nombre de usuario" required autofocus />
                            </br>
                            <input type="password" name="password" class="form-control input-md chat-input" placeholder="Contrase&ntilde;a" required/>
                            </br>
                            <div class="wrapper">
                                <span class="group-btn">     
                                    <button type="submit" class="btn btn-primary btn-md">Entrar    <i class="fa fa-sign-in"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </body>
</html>
