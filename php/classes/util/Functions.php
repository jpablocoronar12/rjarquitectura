<?php

class Functions {
    
    public static function yearsDiff($Date1, $Date2){
        $Date1=new DateTime($Date1);
        $Date2=new DateTime($Date2);
        $Diff=$Date1->diff($Date2, true);
        return $Diff->y;
    }
     /**
     * Obtiene las dimensiones de un pdf
     * @param type String la ruta del archivo
     * @return array arreglo con las dimensiones
     */
      
    public static function get_pdf_dimensions($path,$box="MediaBox") {
    //$box can be set to BleedBox, CropBox or MediaBox 
        $size=filesize($path);
        $result = array();
        $tmp = explode('x', $size);
        $result['height'] = round(trim($tmp[0])/2.83);
        $result['width'] = round(trim($tmp[1])/2.83);
        error_log("heih ".$result['height']. "wid ".$result['width']);
        return $result;
   
/*        
          $output = shell_exec("pdfinfo ".$path);
          $file = new SplFileObject($path);
          $stat = $file->fstat();
          $fontname = PDF_get_value($path, "fontname");
          error_log("Font name ".$fontname);
          // find page sizes
          preg_match('/Page size:\s+([0-9]{0,5}\.?[0-9]{0,3}) x ([0-9]{0,5}\.?[0-9]{0,3})/', $output, $pagesizematches);
          $width = round($pagesizematches[1]/2.83);
          $height = round($pagesizematches[2]/2.83);
          $Dimensions['width']=$width;
          $Dimensions['height']=$height;
          return $Dimensions;*/
    }
 
    

    public static function monthsDiff($Date1, $Date2){
        $Date1=new DateTime($Date1);
        $Date2=new DateTime($Date2);
        $Diff=$Date1->diff($Date2, true);
        $Months=$Diff->y*12;
        $Months+=$Diff->m;
        return $Months;
    }
    
    public static function daysDiff($Date1, $Date2){
        $Date1=new DateTime($Date1);
        $Date2=new DateTime($Date2);
        $Diff=$Date1->diff($Date2, true);
        $Days=$Diff->format("%a");
        return $Days;
    }
    
    public static function dateBetween($DateRef1, $DateRef2, $Date){
        $DateRef1=new DateTime($DateRef1);
        $DateRef2=new DateTime($DateRef2);
        $Date=new DateTime($Date);
        $DateRef1=$DateRef1->getTimestamp();
        $DateRef2=$DateRef2->getTimestamp();
        $Date=$Date->getTimestamp();
        return ($Date>=$DateRef1 && $Date<=$DateRef2);
    }
    
    public static function randomString($Length){
        $Now=new DateTime();
        $Hash=  sha1($Now->getTimestamp());
        return strtoupper(substr($Hash, 0, $Length));
    }
    
}

