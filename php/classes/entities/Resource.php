<?php 

class Resource {

	public $ResourceId=NULL;
	public $ResourceName=NULL;
	public $ContentType=NULL;
	public $Path=NULL;

	public function __construct($StdObject=NULL) {
            if($StdObject!=NULL){
                $Object=  get_object_vars($this);
                foreach($Object as $Attribute=>$Value){
                    if(property_exists($StdObject, $Attribute)){
                        $this->$Attribute=$StdObject->$Attribute;
                    }
                }
            }
        }

	public function setResourceId($ResourceId=NULL){
		$this->ResourceId=$ResourceId;
	}

	public function getResourceId(){
		return $this->ResourceId;
	}

	public function setResourceName($ResourceName=NULL){
		$this->ResourceName=$ResourceName;
	}

	public function getResourceName(){
		return $this->ResourceName;
	}

	public function setContentType($ContentType=NULL){
		$this->ContentType=$ContentType;
	}

	public function getContentType(){
		return $this->ContentType;
	}

	public function setPath($Path=NULL){
		$this->Path=$Path;
	}

	public function getPath(){
		return $this->Path;
	}

	
}