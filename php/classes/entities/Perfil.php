<?php 

class Perfil {

	public $PerfilId=NULL;
	public $PerfilNombre=NULL;
        
        function setPerfilId($PerfilId) {
            $this->PerfilId = $PerfilId;
        }

        function setPerfilNombre($PerfilNombre) {
            $this->PerfilNombre = $PerfilNombre;
        }

                function getPerfilId() {
            return $this->PerfilId;
        }

        function getPerfilNombre() {
            return $this->PerfilNombre;
        }

        	public function __construct($StdObject=NULL) {
            if($StdObject!=NULL){
                $Object=  get_object_vars($this);
                foreach($Object as $Attribute=>$Value){
                    if(property_exists($StdObject, $Attribute)){
                        $this->$Attribute=$StdObject->$Attribute;
                    }
                }
            }
        }

	
}