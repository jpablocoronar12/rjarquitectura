<?php

/**
 * Description of Imagen
 *
 * @author marco
 */
class Imagen {

    public $ImagenId;
    public $Nombre;
    public $Descripcion;
    public $Path;
    public $Type;
    public $AlbumId;

    function getImagenId() {
        return $this->ImagenId;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function getPath() {
        return $this->Path;
    }

    function getType() {
        return $this->Type;
    }

    function getAlbumId() {
        return $this->AlbumId;
    }

    function setImagenId($ImagenId) {
        $this->ImagenId = $ImagenId;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setPath($Path) {
        $this->Path = $Path;
    }

    function setType($Type) {
        $this->Type = $Type;
    }

    function setAlbumId($AlbumId) {
        $this->AlbumId = $AlbumId;
    }

    public function __construct($StdObject = NULL) {
        if ($StdObject != NULL) {
            $Object = get_object_vars($this);
            foreach ($Object as $Attribute => $Value) {
                if (property_exists($StdObject, $Attribute)) {
                    $this->$Attribute = $StdObject->$Attribute;
                }
            }
        }
    }

  

}
