<?php

/**
 * Description of Mensaje
 *
 * @author marco
 */
class Mensaje {

    public $MensajeId;
    public $Nombre;
    public $Telefono;
    public $Contenido;
    public $Fecha;
    public $Email;
    
    function getEmail() {
        return $this->Email;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }

        function getMensajeId() {
        return $this->MensajeId;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getTelefono() {
        return $this->Telefono;
    }

    function getContenido() {
        return $this->Contenido;
    }

    function getFecha() {
        return $this->Fecha;
    }

    function setMensajeId($MensajeId) {
        $this->MensajeId = $MensajeId;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setTelefono($Telefono) {
        $this->Telefono = $Telefono;
    }

    function setContenido($Contenido) {
        $this->Contenido = $Contenido;
    }

    function setFecha($Fecha) {
        $this->Fecha = $Fecha;
    }

    public function __construct($StdObject = NULL) {
        if ($StdObject != NULL) {
            $Object = get_object_vars($this);
            foreach ($Object as $Attribute => $Value) {
                if (property_exists($StdObject, $Attribute)) {
                    $this->$Attribute = $StdObject->$Attribute;
                }
            }
        }
    }

   

}
