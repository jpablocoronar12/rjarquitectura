<?php

/**
 * Description of Album
 *
 * @author marco
 */
class Album  {

    public $AlbumId;
    public $Nombre;
    public $Descripcion;
    public $Logo;

    function getAlbumId() {
        return $this->AlbumId;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function setAlbumId($AlbumId) {
        $this->AlbumId = $AlbumId;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setLogo($Logo){
        $this->Logo = $Logo;
    }

    function getLogo(){
        return $this->Logo;
    }

    public function __construct($StdObject = NULL) {
        if ($StdObject != NULL) {
            $Object = get_object_vars($this);
            foreach ($Object as $Attribute => $Value) {
                if (property_exists($StdObject, $Attribute)) {
                    $this->$Attribute = $StdObject->$Attribute;
                }
            }
        }
    }

    

}
