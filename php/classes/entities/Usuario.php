<?php

/**
 * Description of Usuario
 *
 * @author marco
 */
class Usuario {

    public $UsuarioId;
    public $Nombre;
    public $Login;
    public $Password;

    function getUsuarioId() {
        return $this->UsuarioId;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getLogin() {
        return $this->Login;
    }

    function getPassword() {
        return $this->Password;
    }

    function setUsuarioId($UsuarioId) {
        $this->UsuarioId = $UsuarioId;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setLogin($Login) {
        $this->Login = $Login;
    }

    function setPassword($Password) {
        $this->Password = $Password;
    }

    public function __construct($StdObject = NULL) {
        if ($StdObject != NULL) {
            $Object = get_object_vars($this);
            foreach ($Object as $Attribute => $Value) {
                if (property_exists($StdObject, $Attribute)) {
                    $this->$Attribute = $StdObject->$Attribute;
                }
            }
        }
    }
}
