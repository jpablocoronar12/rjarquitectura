<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PacienteController
 *
 * @author marco
 */
class UsuarioController extends Controller{
    
    public function listaUsuariosAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $User=$this->getSessionValue("user");
        $Params=$this->getJsonRequest();
        $Users = UsuarioDAO::listaUsuarios($User->getUsuarioId(),$Params->PerfilId);
        $this->sendJson($Users);
    }
    
    public function borrarUsuarioAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params=$this->getJsonRequest();
        if(!UsuarioDAO::borrarUsuario($Params->UsuarioId)){
            $this->sendHttpStatus(500, "Ocurrió un error al borrar el usuario.");
        }
    }
    
    public function guardarUsuarioAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Session=$this->getSessionValue("user");
        $Params=$this->getJsonRequest();
        $User = new Usuario($Params);
        $User->setMedicoId($Session->getUsuarioId());
        
        if($User->getNombre()==NULL||$User->getTelefono1()==NULL||$User->getCurp()==NULL){
            $this->sendHttpStatus(500, "Todos los datos son obligatorios verifica por favor.");
            return;
        }
        if(!UsuarioDAO::guardarUsuario($User)){
            $this->sendHttpStatus(500, "Ocurrió un error al guardar el usuario");
        }
    }
    
}
