<?php

/**
 * Description of Resource
 *
 * @author mauricio
 */
class ResourceController extends Controller{
    //put your code here
    public function listResourceAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $resource = ResourceDAO::listResources();
        $this->sendJson($resource);
    }
    
    public function listResourcesLessonAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params=$this->getJsonRequest();
        $resource = ResourceDAO::listResources($Params->LessonId);
        $this->sendJson($resource);
    }
    
    public function getResourceAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params=$this->getJsonRequest();
        $resource = ResourceDAO::getResource($Params->ResourceId);
        $this->sendJson($resource);
    }
    
    public function deleteResourceAction(){
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params=$this->getJsonRequest();
        $resource = ResourceDAO::getResource($Params->ResourceId);
        
        if(file_exists($resource->getPath()))
            if(!unlink($resource->getPath())) 
                $this->sendHttpStatus(500, "Ocurrio un error al borrar el recurso");
            
        if(!LessonResourceDAO::deleteLessonResource($Params->LessonId, $Params->ResourceId))
            $this->sendHttpStatus(500, "Ocurrio un error al borrar el recurso.");
        
        if(!ResourceDAO::deleteResource($Params->ResourceId))
            $this->sendHttpStatus(500, "Ocurrio un error al borrar el recurso.");
    }
    
    
    
    
    public function downloadResourceAction(){
        require_once 'php/libraries/fpdf/fpdf.php';
        require_once 'php/libraries/fpdi/fpdi.php';
        $Perfil = $this->getSessionValue("user")->getPerfilId();
        $Accion =  str_replace("Action", "", __FUNCTION__);
        if(!Auth::validSession($Perfil,$Accion )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        
        $User=$this->getSessionValue("user");
        $ResourceId=$this->getParam("ResourceId");
        
        if($User->getProfileId()!=3){
            $this->setHeader("X-Session-Status", "Forbidden");
            $this->sendHttpStatus(419, "No tiene permiso de acceder a este recurso");
            return;
        }
        
        $resource = ResourceDAO::getResource($ResourceId);
        if (!UserDAO::existResource($User->getUserId(),$ResourceId)){
            $this->setHeader("X-Session-Status", "Forbidden");
            $this->sendHttpStatus(419, "No tiene permiso de acceder a este recurso");
            header("Location: logout.do");
            return;
        }
            
        if($resource->getContentType()==="pdf"){
            $pdf = new FPDI();
            $pdf->AddPage();
            $pdf->setSourceFile($resource->getPath());
            $tplidx = $pdf->importPage(1); 
            $Dimensions = $pdf->getTemplateSize($tplidx); 
            $InstitutionName = InstitutionDAO::getInstitution($User->getInstitutionId())->getInstitutionName();
            
            if($Dimensions['w']>500){
                $pdf = new FPDI('L','mm',array($Dimensions['w'],$Dimensions['h']));
            }
            else{
                $pdf = new FPDI('P','mm',array($Dimensions['w'],$Dimensions['h']));
            }
            $pdf->SetFont('Arial');
            $pdf->SetFontSize($Dimensions['w']/25);
            $pdf->setTextColor(120,133,139);
            $pageCount=$pdf->setSourceFile($resource->getPath());
           
            for($i=1;$i <= $pageCount;$i++){
                 $pdf->addPage();
                 $tplIdx = $pdf->importPage($i);
                 $pdf->useTemplate($tplIdx, 0, 0,$Dimensions['w'],$Dimensions['h']);
                 //$pdf->SetXY($Dimensions['w']/2-($Dimensions['w']/100*30), $Dimensions['h']-40);
                 //$pdf->Write(0,iconv('utf-8','cp1252',"".$User->getFirstName()));
                 $pdf->SetXY(5, ($Dimensions['h']-23));
                 $pdf->Write(0,iconv('utf-8','cp1252',$User->getFirstName().", ".$InstitutionName));
            }
            $pdf->Output(); 
        }
        else{
            $stream = fopen($resource->getPath(), 'r+');
            $this->sendDownload($stream, $resource->getResourceName());
            //header("Location:". $resource->getPath());
        }
        
    }
    
}