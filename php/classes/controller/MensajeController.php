<?php

/**
 * Description of MensajeController
 *
 * @author marco
 */
class MensajeController extends Controller {
    
    public function listarMensajesAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $mensajes = MensajeDAO::listarMensajes();
        $this->sendJson($mensajes);
    }

    public function sendMessageAction(){
        $Params = $this->getJsonRequest();
        $Mensaje = MensajeDAO::MensajeFromParams($Params);
        error_log(var_dump($Mensaje));

        if($Mensaje->getContenido()==NULL OR $Mensaje->getEmail()==NULL 
                OR $Mensaje->getNombre()==NULL OR $Mensaje->getTelefono()==NULL){
            $this->sendHttpStatus(500, "Todos los campos son obligatorios.");
            //return;
        }
        if(!MensajeDAO::saveMensaje($Mensaje)){
            $this->sendHttpStatus(500, "Ocurrio un error al enviar el mensaje, intentelo más tarde.");
        }
    }
    
    public function borrarMensajeAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params = $this->getJsonRequest();
        $MensajeId = $Params->MensajeId;
        if(!MensajeDAO::borrarMensaje($MensajeId)){
            $this->sendHttpStatus(500, "Ocurrio un error al borrar el mensaje.");
        }
    }
    
}
