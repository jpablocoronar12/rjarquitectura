<?php

class IndexController extends Controller {
    
    public function indexAdminAction(){
        if(Auth::validSession()){
            $this->redirect("html/views/admin/app.html");
            return;
        }
    }
    public function indexUserAction(){
        if(Auth::validSession()){
            $this->redirect("index.html");
            return;
        }
    }
}

