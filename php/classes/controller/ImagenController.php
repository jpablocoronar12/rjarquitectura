<?php

/**
 * Description of ImagenController
 *
 * @author marco
 */
class ImagenController extends Controller {
    public function borrarImagenAction(){
        
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        
        $Params=$this->getJsonRequest();
        
        $Imagen = ImagenDAO::getImagen($Params->ImagenId);

        if(file_exists($Imagen->getPath())){
               if(!unlink($Imagen->getPath())){
                   $this->sendHttpStatus(500, "Ocurrio un error al borrar la imagen");
                   return;
               }
        }
        if(!ImagenDAO::borrarImagen($Imagen->getImagenId())){
            $this->sendHttpStatus(500, "Ocurrio un error al borrar la imagen");
        }
    }
    
    public function uploadImagenAction(){
        
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        
        $file = $this->getFileParam("file");
        $Imagen = new Imagen();
        $AlbumId = $this->getParam('AlbumId');

        $IsLogoAlbum = $this->getParam('IsLogoAlbum');
        if($IsLogoAlbum){
            $Album = AlbumDAO::getAlbum($AlbumId);
            $Type = pathinfo($file["name"],PATHINFO_EXTENSION);

            if(file_exists($Album->getLogo())){
                if(!unlink($Album->getLogo())){
                   $this->sendHttpStatus(500, "Ocurrio un error al borrar la imagen");
                   return;
               }
            }
            $Album->setLogo("resources/" . $timestamp . "." . $Type);

            if (move_uploaded_file($_FILES["file"]["tmp_name"], $Album->getLogo())){
                $this->sendHttpStatus(200 ,"El recurso ha sido subido correctamente.");
            }
            else {
                $this->sendHttpStatus(500, "Lo siento el recurso no ha podido ser subido.");
                return;
            }
            if(!AlbumDAO::guardarAlbum($Album)){
                $this->sendHttpStatus(500, "Ocurrio un error al guardar el album");
                return;
            }
        }
        else{

            $Imagen->setAlbumId($AlbumId);
            $Imagen->setNombre($file["name"]);
            $Imagen->setType(pathinfo($file["name"],PATHINFO_EXTENSION));
            $Imagen->setPath("resources/" . $timestamp . "." . $Imagen->getType());
            

            if (move_uploaded_file($_FILES["file"]["tmp_name"], $Imagen->getPath())){
                $this->sendHttpStatus(200 ,"El recurso ha sido subido correctamente.");
            }
            else {
                $this->sendHttpStatus(500, "Lo siento el recurso no ha podido ser subido.");
                return;
            }
           
            if(!ImagenDAO::guardarImagen($Imagen))
                $this->sendHttpStatus(500, "Ocurrio un error al guardar la imagen.");
        }   
    }
}
