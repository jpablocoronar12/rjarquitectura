<?php

/**
 * Description of AlbumController
 *
 * @author marco
 */
class AlbumController extends Controller  {
    
    public function getAlbumsAction(){
        $Albums = AlbumDAO::listarAlbums();
        $this->sendJson($Albums);
    }
    
    public function getImagesFromAlbumAction(){
        $Params=$this->getJsonRequest();
        $Imagenes = AlbumDAO::getImagenes($Params->AlbumId);
        $this->sendJson($Imagenes);
    }
    public function guardarAlbumAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params=$this->getJsonRequest();
        $Album=new Album($Params);
        if($Album->getNombre()==NULL){
            $this->sendHttpStatus(500, "El nombre es obligatorio.");
            return;
        }
        if(!AlbumDAO::guardarAlbum($Album)){
            $this->sendHttpStatus(500, "Ocurrio un error al guardar la institución");
            return;
        }
    }
    
    public function getAlbumAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params=$this->getJsonRequest();
        $Album = AlbumDAO::getAlbum($Params->AlbumId);
        $this->sendJson($Album);
    }
    public function borrarAlbumAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params = $this->getJsonRequest();
        $AlbumId = $Params->AlbumId;
        if(!AlbumDAO::borrarAlbum($AlbumId)){
            $this->sendHttpStatus(500, "Ocurrio un error al borrar el mensaje.");
        }
    }
    public function getImagenesAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Params = $this->getJsonRequest();
        $AlbumId = $Params->AlbumId;
        $Imagenes = AlbumDAO::getImagenes($AlbumId);
        $this->sendJson($Imagenes);
    }
    
    
    public function listarAlbumsAction(){
        if(!Auth::validSession()){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $Albums = AlbumDAO::listarAlbums();
        $this->sendJson($Albums);
    }
}
