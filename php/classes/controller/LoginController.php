<?php

class LoginController extends Controller {
    
    public function loginIndexAction(){
        $this->sendView("html/views/admin/loginView.php");
    }
    
    public function loginAction(){
        $Vars=new VariableHolder();
        $User=$this->getParam("user");
        $Password=$this->getParam("password");
        $Usuario=UsuarioDAO::getUserLogon($User, $Password);
        if($Usuario!==NULL){
            $this->setSessionValue("user", $Usuario);
            $this->redirect("indexAdmin.do");
        } else {
            $Vars->ErrorMessage="Nombre de Usuario o contrase&ntilde;a incorrectos";
            $this->sendView("html/views/admin/loginView.php", $Vars);
        }
    }
    
    public function logoutAction(){
        $_SESSION = array();
        // sends as Set-Cookie to invalidate the session cookie
        if (filter_input(INPUT_COOKIE, session_name())!=NULL) { 
            $params = session_get_cookie_params();
            setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
        }
        session_destroy();
        $this->redirect("loginIndex.do");
    }
    
    public function checkAuthAction(){
        if(!Auth::validSession( )){
            $this->setHeader("X-Session-Status", "Timeout");
            $this->sendHttpStatus(419, "Su Sesion ha expirado");
            return;
        }
        $User= UsuarioDAO::getUser($this->getSessionValue("user")->getUsuarioId());
        $this->sendJson($User);
    }
    
}

