<?php

class Auth {
    
    public static function validSession($ProfileId=NULL,$Action=NULL){
        $ArregloActividades = array(
            "1"=>array("checkAuth","uploadResource","listaUsuarios","borrarUsuario","guardarUsuario"),
            "2"=>array("checkAuth"),
            "3"=>array("checkAuth"));
        
        $Controller=new Controller();
        $Usuario=$Controller->getSessionValue("user");
        if($Usuario==NULL){
            $Controller->redirect("loginIndex.do");
            return false;
        }
        if($ProfileId!=NULL AND $Action!=NULL){
            if(!in_array($Action,$ArregloActividades[$ProfileId])){
               $LoginController = new LoginController();
               $LoginController->logoutAction();
               
               $Controller->redirect("loginIndex.do");
               return false;
            }    
        }
        return true;
    }
    
}
