<?php

class ResourceDAO {
    
    public static function ResourceFromVar($Var){
        $Resource=new Resource();
        $Resource->setResourceId($Var['ResourceId']);
	$Resource->setResourceName($Var['ResourceName']);
	$Resource->setContentType($Var['ContentType']);
	$Resource->setPath($Var['Path']);
        return $Resource;
    }
    
    public static function listResources($LessonId=NULL, $SearchString=NULL, $PageNumber=NULL, $PageSize=NULL){
        $Resources=array();
        $Query="select Resource.*"
            . " from Resource"
            . " join LessonResource on Resource.ResourceId = LessonResource.ResourceId"
            . " where 1=1";
        if($LessonId !== NULL){
            $Query .= " and LessonResource.LessonId=$LessonId";
        }
        if($SearchString!=NULL){
            $SearchString=str_replace(" ", "%", $SearchString);
            $Query.=" and like '%$SearchString%'";
        }
    	if(!($PageNumber===NULL || $PageSize===NULL)){
            $LimitStart = $PageSize * $PageNumber + 1;
            $PageSize++;
            $Query.="limit $LimitStart, $PageSize";
        }
        $DB=DB::getInstance();
        $Result=$DB->Select($Query);
        foreach($Result as $Row){
            $Resource=self::ResourceFromVar($Row);
            $Resources[]=$Resource;
        }
        $DB->Close();
        return $Resources;
    }
    
    public static function getResource($ResourceId){
        $Resource=NULL;
        $Query="select *"
                . " from Resource "   
                . " where Resource.ResourceId=$ResourceId";
        $DB=DB::getInstance();
        $Result=$DB->Select($Query);
        foreach($Result as $Row){
            $Resource=ResourceDAO::ResourceFromVar($Row);
        }
        $DB->Close();
        return $Resource;
    }
    
    public static function saveResource(Resource $Resource){
        $Query="insert into Resource(ResourceId,ResourceName,ContentType,Path) "
                . " values(?,?,?,?) on duplicate key update "
                . " ResourceId=values(ResourceId),ResourceName=values(ResourceName),ContentType=values(ContentType),Path=values(Path) ";
        $DB=DB::getInstance();
        $ps=$DB->Prepare($Query);
        $ps->bind_param("isss", 
                $Resource->getResourceId(),$Resource->getResourceName(),$Resource->getContentType(),$Resource->getPath()
                );
        $result=$ps->execute();
        if($result){
            $Resource->setResourceId($DB->getLastInsertId());
        }
        $DB->Close();
        return $result;
    }
    
    public static function deleteResource($ResourceId){
        $Query="delete "
                . " from Resource "
                . " where ResourceId=$ResourceId";
        $DB=DB::getInstance();
        $Result=$DB->Execute($Query);
        $DB->Close();
        return $Result;
    }    
}    
