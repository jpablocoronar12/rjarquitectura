<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioDAO
 *
 * @author marco
 */
class UsuarioDAO {
    
    public static function UserFromVar($Var){
        $User=new Usuario();
        $User->setUsuarioId($Var['UsuarioId']);
        $User->setNombre($Var['Nombre']);
        $User->setLogin($Var['Login']);
        $User->setPassword($Var['Password']);
        return $User;
    }
    
    public static function getUserLogon($UserName, $Password){
        $User=NULL;
        $Query = "select *
            from Usuario u
            where u.Login='".DB::escape($UserName)."'
            and u.Password='".DB::escape($Password)."'";
        $DB=DB::getInstance();
        $Rows=$DB->Select($Query);
        $DB->Close();
        foreach($Rows as $Row){
            $User= UsuarioDAO::UserFromVar($Row);
        }
        return $User;
    }
    public static function getUser($UserId){
        $User=NULL;
        $Query="select * "
                . " from Usuario "
                . " where UsuarioId={$UserId}";
        $DB=DB::getInstance();
        $Result=$DB->Select($Query);
        foreach($Result as $Row){
            $User=self::UserFromVar($Row);
        }
        $DB->Close();
        return $User;
    }
}
