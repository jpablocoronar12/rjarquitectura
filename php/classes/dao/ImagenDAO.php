<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImagenDAO
 *
 * @author marco
 */
class ImagenDAO {

    public static function ImagenFromVar($Var) {
        $Imagen = new Imagen();
        $Imagen->setImagenId($Var['ImagenId']);
        $Imagen->setNombre($Var['Nombre']);
        $Imagen->setDescripcion($Var['Descripcion']);
        $Imagen->setAlbumId($Var['AlbumId']);
        $Imagen->setPath($Var['Path']);
        $Imagen->setType($Var['Type']);
        return $Imagen;
    }
    
    public static function getImagen($ImagenId){
        $Imagen=NULL;
        $Query="select *"
                . " from Imagen "   
                . " where ImagenId=$ImagenId";
        $DB=DB::getInstance();
        $Result=$DB->Select($Query);
        foreach($Result as $Row){
            $Imagen=  ImagenDAO::ImagenFromVar($Row);
        }
        $DB->Close();
        return $Imagen;
    }
    public static function borrarImagen($ImagenId) {
        $Query = "delete "
                . " from Imagen "
                . " where ImagenId=$ImagenId";
        $DB = DB::getInstance();
        $Result = $DB->Execute($Query);
        $DB->Close();
        return $Result;
    }

    public static function guardarImagen(Imagen $Imagen) {
        $Query = "insert into Imagen(ImagenId,Nombre,Descripcion,Path,Type,AlbumId) "
                . " values(?,?,?,?,?,?) on duplicate key update "
                . " ImagenId=values(ImagenId),Nombre=values(Nombre),Descripcion=values(Descripcion),"
                . "Path=values(Path),Type=values(Type),AlbumId=values(AlbumId) ";
        $DB = DB::getInstance();
        $ps = $DB->Prepare($Query);
        $ps->bind_param("issssi", $Imagen->getImagenId(), $Imagen->getNombre(), $Imagen->getDescripcion(), $Imagen->getPath(), $Imagen->getType(), $Imagen->getAlbumId()
        );
        $result = $ps->execute();
        $DB->Close();
        return $result;
    }

}
