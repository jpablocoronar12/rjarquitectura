<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MensajeDAO
 *
 * @author marco
 */
class MensajeDAO {
    
    public static function MensajeFromParams($Var){
            $Mensaje=new Mensaje();
            $Mensaje->setNombre($Var->Nombre);
            $Mensaje->setEmail($Var->Email);
            $Mensaje->setTelefono($Var->Telefono);
            $Mensaje->setContenido($Var->Contenido);
            $Mensaje->setFecha($Var->Fecha);
            $Mensaje->setMensajeId($Var->MensajeId);
            return $Mensaje;
        }
        
        public static function MensajeFromVar($Var){
            $Mensaje=new Mensaje();
            $Mensaje->setNombre($Var['Nombre']);
            $Mensaje->setEmail($Var['Email']);
            $Mensaje->setTelefono($Var['Telefono']);
            $Mensaje->setContenido($Var['Contenido']);
            $Mensaje->setFecha($Var['Fecha']);
            $Mensaje->setMensajeId($Var['MensajeId']);
            return $Mensaje;
        }
        
        public static function borrarMensaje($MensajeId){
            $Query="delete "
                    . " from Mensaje "
                    . " where MensajeId=$MensajeId";
            $DB=DB::getInstance();
            $Result=$DB->Execute($Query);
            $DB->Close();
            return $Result;
        } 
        
        public static function saveMensaje(Mensaje $Mensaje){
            $Query="insert into Mensaje(Nombre,Telefono,Contenido,Email) "
                    . " values(?,?,?,?)";
            $DB=DB::getInstance();
            $ps=$DB->Prepare($Query);
            $ps->bind_param("ssss", $Mensaje->getNombre(),$Mensaje->getTelefono(),
                    $Mensaje->getContenido(),$Mensaje->getEmail()
                    );
            $result=$ps->execute();
            $DB->Close();
            return $result;
        }
        
        public static function listarMensajes(){
            $Mensajes=array();
            $Query="select * "
                    . " from Mensaje ";
            $DB=DB::getInstance();
            $Result=$DB->Select($Query);
            foreach($Result as $Row){
                $mensaje = MensajeDAO::MensajeFromVar($Row);
                $Mensajes[] = $mensaje;
            }
            $DB->Close();
            return $Mensajes;
        }
}
