<?php

/**
 * Description of AlbumDAO
 *
 * @author marco
 */
class AlbumDAO {

    public static function AlbumFromVar($Var) {
        $Album = new Album();
        $Album->setAlbumId($Var['AlbumId']);
        $Album->setNombre($Var['Nombre']);
        $Album->setDescripcion($Var['Descripcion']);
        $Album->setLogo($Var['Logo']);
        return $Album;
    }

    public static function listarAlbums() {
        $Albums = array();
        $Query = "select * "
                . " from Album ";
        $DB = DB::getInstance();
        $Result = $DB->Select($Query);
        foreach ($Result as $Row) {
            $Album = AlbumDAO::AlbumFromVar($Row);
            $Albums[] = $Album;
        }
        $DB->Close();
        return $Albums;
    }

    public static function getImagenes($AlbumId) {
        $Imagenes = array();
        $Query = "select * "
                . " from Imagen "
                . " where AlbumId=$AlbumId";
        
        $DB = DB::getInstance();
        $Result = $DB->Select($Query);
        foreach ($Result as $Row) {
            $Imagen = ImagenDAO::ImagenFromVar($Row);
            $Imagenes[] = $Imagen;
        }
        $DB->Close();
        return $Imagenes;
    }

    public static function getAlbum($AlbumId) {
        $Album = NULL;
        $Query = "select * "
                . " from Album "
                . " where AlbumId=$AlbumId";
        $DB = DB::getInstance();
        $Result = $DB->Select($Query);
        foreach ($Result as $Row) {
            $Album = self::AlbumFromVar($Row);
        }
        $DB->Close();
        return $Album;
    }

    public static function guardarAlbum(Album $Album) {
        $Query = "insert into Album(AlbumId,Nombre,Descripcion,Logo) "
                . " values(?,?,?,?) on duplicate key update "
                . " AlbumId=values(AlbumId),Nombre=values(Nombre),"
                . " Descripcion=values(Descripcion), Logo=values(Logo) ";
        $DB = DB::getInstance();
        $ps = $DB->Prepare($Query);
        $ps->bind_param("isss", $Album->getAlbumId(), $Album->getNombre(), $Album->getDescripcion(),$Album->getLogo()
        );
        $result = $ps->execute();
        $DB->Close();
        return $result;
    }

    public static function borrarAlbum($AlbumId) {
        $Query = "delete "
                . " from Album "
                . " where AlbumId=$AlbumId";
        $DB = DB::getInstance();
        $Result = $DB->Execute($Query);
        $DB->Close();
        return $Result;
    }

}
