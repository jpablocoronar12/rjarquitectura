<?php

// The actions File used to determine the controller and 
// the method to execute

$IndexController=new IndexController();
$APP_ACTIONS["indexAdmin"]=$IndexController;
$APP_ACTIONS["indexUser"]=$IndexController;

// Acciones para inicio/cierre de Sesion
$LoginController=new LoginController();
$APP_ACTIONS["loginIndex"]=$LoginController;
$APP_ACTIONS["login"]=$LoginController;
$APP_ACTIONS["logout"]=$LoginController;
$APP_ACTIONS["checkAuth"]=$LoginController;

$MessageController = new MensajeController();
$APP_ACTIONS["listarMensajes"]=$MessageController;
$APP_ACTIONS["borrarMensaje"]=$MessageController;
$APP_ACTIONS["sendMessage"]=$MessageController;

$AlbumController = new AlbumController();
$APP_ACTIONS["guardarAlbum"]=$AlbumController;
$APP_ACTIONS["listarAlbums"]=$AlbumController;
$APP_ACTIONS["getAlbum"]=$AlbumController;
$APP_ACTIONS["borrarAlbum"]=$AlbumController;
$APP_ACTIONS["getImagenes"]=$AlbumController;
$APP_ACTIONS["getAlbums"]=$AlbumController;
$APP_ACTIONS["getImagesFromAlbum"]=$AlbumController;




$ImagenController = new ImagenController();
$APP_ACTIONS["uploadImagen"]=$ImagenController;
$APP_ACTIONS["borrarImagen"]=$ImagenController;

$ResourceController = new ResourceController();
$APP_ACTIONS["listResource"]=$ResourceController;
$APP_ACTIONS["listResourcesLesson"]=$ResourceController;
$APP_ACTIONS["saveResource"]=$ResourceController;
$APP_ACTIONS["getResource"]=$ResourceController;
$APP_ACTIONS["deleteResource"]=$ResourceController;
$APP_ACTIONS["uploadResource"]=$ResourceController;
$APP_ACTIONS["downloadResource"]=$ResourceController;
