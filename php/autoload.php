<?php

function project_autoloader($Class){
    $BaseDir=dirname(__FILE__);
    $ClassPath="{$BaseDir}/classes";

    foreach(explode(PATH_SEPARATOR,$ClassPath) as $Path){
        $DirContent=scandir($Path);
        if($DirContent!==false){
            foreach($DirContent as $Entry){
                $FullEntry=$Path.DIRECTORY_SEPARATOR.$Entry;
                if(file_exists("$FullEntry/{$Class}.php")){
                    include_once "$FullEntry/{$Class}.php";
                    return ;
                }
            }
        }
    }
}

spl_autoload_register('project_autoloader');